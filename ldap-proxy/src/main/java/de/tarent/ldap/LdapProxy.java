/*
 * LDAP-Proxy,
 * A lib to use LDAP
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'LDAP-Proxy'
 * Signature of Elmar Geese, 27 June 2007
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.ldap;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;

import de.tarent.ldap.query.Expression;
import de.tarent.ldap.query.InvalidQueryException;
import de.tarent.ldap.query.LdapQuery;
import de.tarent.ldap.query.QueryObject;

public class LdapProxy {
	/** Instances of this Class */
	protected static Hashtable instances = new Hashtable();
	
	/** Host to Connect (example: ldap://ldap.example.org/ */
	protected String host;
	
	/** The path in Directory where search should beginn */
	protected List searchPath;
	
	
	/*************************************************************
	 * 						Constructor							 *
	 *************************************************************/
	protected LdapProxy(){}
	
	protected LdapProxy(String host){
		this.host = host;
		this.searchPath = Collections.synchronizedList(new ArrayList());
	}
	
	protected LdapProxy(String host, List searchPaths){
		this(host);
		this.searchPath = searchPaths;
	}
	
	
	/**
	 * This method returns a Hashtable with all LDAP-attributes of a LDAP-user
	 * 
	 * @param dnUser The DN of a user
	 * @return Hashtable with all user-attributes
	 */
	public Hashtable getAttributes(String dnUser){
		Hashtable htAttributes = new Hashtable();
		try {
			DirContext ctx = this.getDirContext();
			Attributes attributes = ctx.getAttributes(dnUser);
			NamingEnumeration enumeration = (NamingEnumeration)(attributes.getAll());
			while(enumeration.hasMore()){
				Attribute temp = (Attribute)enumeration.next();
				Object value = temp.get();
				if(value.getClass().getName().equals("java.lang.String")){
					htAttributes.put(temp.getID().toString(),temp.get().toString());
				}else{
					//TODO Querry other Types 
					if(value.getClass().getName().equals("[B")){
						htAttributes.put(temp.getID(), new String((byte[])(temp.get())));
					}
				}
			}
			ctx.close();
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return htAttributes;
	}
	
	/**
	 * This method returns looks up the DN of a user
	 * 
	 * @param query Ldap query, which filters result
	 * @return An Arraylist with the DNs
	 */
	synchronized public List getDn(QueryObject query){
		List list = Collections.synchronizedList(new ArrayList());
		
		if(this.searchPath == null || this.searchPath.size() == 0)
			list = getDn(query, "");
		
		else {
			
			Iterator pathes = searchPath.iterator();
			
			while(pathes.hasNext())
				list.addAll(this.getDn(query, (String)(pathes.next())));
			
		}
		return list;
	}
	
	/**
	 * This method returns looks up the DN of a user
	 * 
	 * @param userName The value to search for
	 * @param type The name of the type in LDAP (uid, cn, etc)
	 * @return An Arraylist with the DNs
	 */
	synchronized public List getDn(String userName, String type){
		return getDn(getQueryForTypeAndUsername(userName, type));
	}
	
	/**
	 * This method returns looks up the DN of a user
	 * 
	 * @param userName The value to search for
	 * @param type The name of the typ in LDAP (uid, cn, etc)
	 * @param searchPath The Path in Directory, where search schould begin.
	 * @return An Arraylist with the DNs
	 */
	synchronized public List getDn(String userName, String type, String searchPath){
		LdapQuery query = new LdapQuery();
		query.addContent(new Expression(type, Expression.equals, userName));
		return getDn(query, searchPath);
	}
	
	/**
	 * This method returns looks up the DN of a user
	 * 
	 * @param query A LdapQuery, which filters the results.
	 * @param searchPath The Path in Directory, where search schould begin.
	 * @return An Arraylist with the DNs
	 */
	synchronized public List getDn(QueryObject query, String searchPath){
		List list = Collections.synchronizedList(new ArrayList());
		try {
			DirContext ctx = this.getDirContext();
			SearchControls sc = new SearchControls();
			sc.setSearchScope(SearchControls.SUBTREE_SCOPE);
			NamingEnumeration sr = ctx.search(searchPath, query.generateObjectString() , sc);
			while(sr.hasMoreElements()){
				list.add(((SearchResult)(sr.next())).getNameInNamespace());
			}
			ctx.close();
		} catch (NamingException e) {
			// 
			e.printStackTrace();
		} catch (InvalidQueryException e) {
			// 
			e.printStackTrace();
		}
		return list;
	}
	
	/**
	 * This method authenticates a user by uid on a LDAP-Server
	 * 
	 * @param username The name of the user (uid)
	 * @param pwd The password of the user 
	 * @param authType The typ of authentication (simple etc)
	 * @return true if authentication was successful
	 */
	public boolean authenticateByUid(String username, String pwd, String authType){
		List dns = this.getDn(username, "uid");
		boolean state = false;
		for(int i = 0; i < dns.size(); i++){
			if(this.authenticateByDn((String)(dns.get(i)), pwd, authType)){
				state = true;
			}
		}
		return state;
	}
	
	/**
	 * This method authenticates a user by cn on a LDAP-Server
	 * 
	 * @param username The name of the user (cn)
	 * @param pwd The password of the user 
	 * @param authType The type of authentication (simple etc)
	 * @return true if authentication was successful
	 */
	public boolean authenticateByUserName(String username, String pwd, String authType){
		List dns = this.getDn(username, "cn");
		boolean state = false;
		for(int i = 0; i < dns.size(); i++){
			if(this.authenticateByDn((String)(dns.get(i)), pwd, authType)){
				state = true;
			}
		}
		return state;
	}
	
	/**
	 * This method authenticates a user by DN on a LDAP-Server
	 * 
	 * @param username The name of the user (DN)
	 * @param pwd The password of the user 
	 * @param authType The type of authentication (simple etc)
	 * @return true if authentication was successful
	 */
	public boolean authenticateByDn(String username, String pwd, String authType){
		try{	    	
	    	new InitialDirContext(this.getSecureEnvHashTable(username, pwd, authType));
	    	return true;
		}catch(NamingException e){
			e.printStackTrace();
			return false;
		}
	}
	
	/**
	 * Method to get an anonymous instance of the LDAP-Proxy 
	 * 
	 * @param host The ldpap-host with protocol (example: ldap://ldap.example.org)

	 * @return anonymos LDAPProxy-instance
	 */
	public static LdapProxy getInstance(String host){
		if(!(instances.containsKey(host))){
			instances.put(host, new LdapProxy(host));
		}
		return (LdapProxy)(instances.get(host));
	}
	
	/**
	 * Method to get an anonymous instance of the LDAP-Proxy 
	 * 
	 * @param host The ldpap-host with protocol (example: ldap://ldap.example.org)
	 * @param searchPath a list of search-Path-strings where to search
	 * @return anonymos LDAPProxy-instance
	 */
	public static LdapProxy getInstance(String host, List searchPaths){
		if(!(instances.containsKey(host)))
			instances.put(host, new LdapProxy(host, searchPaths));
		
		return (LdapProxy)(instances.get(host));
	}
	
	/**
	 * Method to get an binded instance of the LDAP-Proxy
	 * 
	 * @param host The ldpap-host with protocol (example: ldap://ldap.example.org)
	 * @param username The name of the user (DN)
	 * @param pwd The password of the user
	 * @return binded LDAPProxy-instance
	 */
	public static LdapProxy getAuthenticatedInstance(String host, String username, String pwd){
		if(!(instances.containsKey(host+"_"+username))){
			instances.put(host+"_"+username, new LdapProxySecure(host, username, pwd, "simple", null));
		}
		return (LdapProxy)(instances.get(host+"_"+username));
	}
	
	/**
	 * Method to get an binded instance of the LDAP-Proxy
	 * 
	 * @param host The ldpap-host with protocol (example: ldap://ldap.example.org)
	 * @param username The name of the user (DN)
	 * @param pwd The password of the user
	 * @param 
	 * @return binded LDAPProxy-instance
	 */
	public static LdapProxy getAuthenticatedInstance(String host, String username, String pwd, List searchPath){
		if(!(instances.containsKey(host+"_"+username))){
			instances.put(host+"_"+username, new LdapProxySecure(host, username, pwd, "simple", searchPath));
		}
		return (LdapProxy)(instances.get(host+"_"+username));
	}
	
	/**
	 * This method gets the Env-Table for an unbinded LDAPProxy
	 * 
	 * @return Environment Hashtable
	 */
    protected Hashtable getEnvHashTable(){
		return this.getDefaultEnvHashTable();
    }
    
    private Hashtable getDefaultEnvHashTable(){
    	Hashtable env = new Hashtable();
    	env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
		env.put(Context.PROVIDER_URL, this.host);
		return env;
    }
	
	/**
	 * This method gets the Env-Table for an unbinded LDAPProxy
	 * 
	 * @param username The name of the user (DN)
	 * @param pwd The password of the user
	 * @param authType The type of authentication (Simple etc)
	 * @return
	 */
	protected Hashtable getSecureEnvHashTable(String username, String pwd, String authType){
		Hashtable env = this.getDefaultEnvHashTable();
		env.put(Context.SECURITY_AUTHENTICATION, authType);
    	env.put(Context.SECURITY_PRINCIPAL, username);
    	env.put(Context.SECURITY_CREDENTIALS, pwd);
    	return env;
	}
	
	/**
	 * This method creates a DirContext. 
	 * It is needed because you connot cache one DirContext, because you can't cache one DirContext (it's closed automatically)
	 * 
	 * @return A DirContext to LDAP
	 */
	protected DirContext getDirContext(){
		DirContext ctx = null;
		try {
			ctx = new InitialDirContext(this.getEnvHashTable());
		} catch (NamingException e) {
			// 
			e.printStackTrace();
		}
		return ctx;
	}	
	
	/**
	 * This method returns a generated LdapQuery for a userName and type
	 * 
	 * @param userName The name of user
	 * @param type Name of the LdapAttribute, which should match
	 * @return Generated LdapQuery
	 */
	public LdapQuery getQueryForTypeAndUsername(String userName, String type){
		LdapQuery query = new LdapQuery();
		query.addContent(new Expression(type, Expression.equals, userName));
		return query;
	}
	
	/**
	 * Method to add a SearchPath to searchPath-ArrayList
	 * 
	 * @param searchPath Path where search should begin
	 */
	public void addSearchPath(String searchPath){
		this.searchPath.add(searchPath);
	}
	
	/**
	 * Method to add SearchPathes to searchPath-ArrayList
	 * 
	 * @param searchPathes String Array with Searchpathes
	 */
	public void addSearchPathes(String[] searchPathes){
		for(int i = 0; i < searchPathes.length; i++){
			this.addSearchPath(searchPathes[i]);
		}
	}
}
