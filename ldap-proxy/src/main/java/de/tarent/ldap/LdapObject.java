/*
 * LDAP-Proxy,
 * A lib to use LDAP
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'LDAP-Proxy'
 * Signature of Elmar Geese, 27 June 2007
 * Elmar Geese, CEO tarent GmbH.
 */
package de.tarent.ldap;

import java.util.Enumeration;
import java.util.Hashtable;

public class LdapObject {
	
	/** Hashtable with attributes of a Object */
	protected Hashtable attributes;
	
	public LdapObject(){
		this.attributes = new Hashtable();
	}
	
	/**
	 * Constructor which creates a object by LDAP-DN
	 * 
	 * @param proxy Proxy which is used to get data
	 * @param dn The DN of the user
	 */
	public LdapObject(LdapProxy proxy, String dn){
		this.attributes = proxy.getAttributes(dn);
	}
	
	/**
	 * Get a special attribute from a object
	 * 
	 * @param name The name of the attribute
	 * @return The attribute value
	 */
	public String getAttribute(String name){
		return (String)(this.attributes.get(name));
	}
	
	public String toString() {
		StringBuffer attributes = new StringBuffer(this.getClass().getName());
		Enumeration keys = this.attributes.keys();
		Enumeration values = this.attributes.elements();
		while(keys.hasMoreElements()){
			attributes.append(keys.nextElement() + ":\t" + values.nextElement() + "\n");
		}
		return attributes.toString();
	}
}
