/*
 * LDAP-Proxy,
 * A lib to use LDAP
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'LDAP-Proxy'
 * Signature of Elmar Geese, 27 June 2007
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.ldap.user; 

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import de.tarent.ldap.LdapObject;
import de.tarent.ldap.LdapProxy;
import de.tarent.ldap.query.Expression;
import de.tarent.ldap.query.LdapQuery;
import de.tarent.ldap.query.Operator;

/**
 * @author David Goemans, Tarent GmbH
 */
public class LdapUser extends LdapObject {
	
	/**
	 * Constructor without parameter
	 */
	public LdapUser(){}

	/**
	 * Constructor with parameter uid
	 * 
	 * @deprecated
	 * @param uid Uid of the user 
	 */
	public LdapUser(String uid){
		this();
		this.attributes.put("uid", uid);
	}
	
	/**
	 * Constructor which creates a user by LDAP-DN
	 * 
	 * @param proxy Proxy which is used to get data
	 * @param dn The DN of the user
	 */
	public LdapUser(LdapProxy proxy, String dn){
		super(proxy, dn);
	}
	
	/**
	 * Constructor which creates a user by type + username
	 * 
	 * @param username The attribute to search for (value)
	 * @param type The attribute to search for (type)
	 */
	public LdapUser(LdapProxy proxy, String username, String type){
		List list = proxy.getDn(username, type);
		Iterator iterator = list.iterator();
		while(iterator.hasNext()){
			String dn = (String)(iterator.next());
			if(dn.contains("ou=people")){
				this.attributes = proxy.getAttributes(dn);
				break;
			}
		}
	}
		
	public boolean equals(Object obj) {
		// TODO Auto-generated method stub
		if(obj instanceof LdapUser) {
			LdapUser temp = (LdapUser)obj;
			return this.attributes.equals(temp.attributes);
		}else{
			return super.equals(obj);	
		}
	}
	
	/**
	 * This method returns all user from a LDAP-server under a defined path
	 * 
	 * @param proxy The proxy which builds the connection
	 * @param searchPath The path in which is searched
	 * @return A list of found LdapUser
	 */
	public static List getAllLdapUser(LdapProxy proxy, String searchPath){
		List userList = Collections.synchronizedList(new ArrayList());
		LdapQuery q = new LdapQuery();
		Operator o = new Operator(Operator.and);
		Operator o2 = new Operator(Operator.not);
		o2.addContent(new Expression("objectClass", Expression.equals, "gosaUserTemplate"));
		o.addContent(new Expression("objectClass", Expression.equals, "gosaMailAccount"));
		o.addContent(o2);
		q.addContent(o);
		List list = proxy.getDn(q, searchPath);
		Iterator iter = list.iterator();
		while(iter.hasNext()){
			userList.add(new LdapUser(proxy, (String)(iter.next())));
		}
		return userList;
	}

}
