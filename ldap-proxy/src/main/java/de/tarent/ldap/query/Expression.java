/*
 * LDAP-Proxy,
 * A lib to use LDAP
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'LDAP-Proxy'
 * Signature of Elmar Geese, 27 June 2007
 * Elmar Geese, CEO tarent GmbH.
 */
package de.tarent.ldap.query;

public class Expression implements QueryObject{
	
	public final static String equals = "=";
	public final static String less = "<=";
	public final static String greater = ">=";
	
	private String connector;
	private String[] parts = new String[2];
	
	public Expression(String part1, String connector, String part2){
		this.parts[0] = part1;
		this.connector = connector;
		this.parts[1] = part2;
	}
	
	public Expression(String[] parts, String connector){
		this.parts = parts;
		this.connector = connector;
	}

	public String generateObjectString() throws InvalidQueryException{
		StringBuffer expression = new StringBuffer();
		expression.append("(");
		expression.append(this.parts[0]);
		expression.append(this.connector);
		expression.append(this.parts[1]);
		expression.append(")");
		return expression.toString();
	}
}
