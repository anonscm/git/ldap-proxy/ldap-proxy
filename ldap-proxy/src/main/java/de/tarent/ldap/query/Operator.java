/*
 * LDAP-Proxy,
 * A lib to use LDAP
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'LDAP-Proxy'
 * Signature of Elmar Geese, 27 June 2007
 * Elmar Geese, CEO tarent GmbH.
 */
package de.tarent.ldap.query;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class Operator implements QueryObject{
	
	public final static String and = "&";
	public final static String or = "|";
	public final static String not = "!";
	
	private String operator;
	private List content;
	
	public Operator(String operator){
		this.operator = operator;
		this.content = Collections.synchronizedList(new ArrayList());
	}
	
	public void addContent(QueryObject queryObject){
		this.content.add(queryObject);
	}
	
	public QueryObject getContent(int index){
		return (QueryObject)(this.content.get(index));
	}
	
	public String generateObjectString() throws InvalidQueryException{
		StringBuffer part = new StringBuffer();
		part.append("(");
		part.append(this.operator);
		
		Iterator contentParts = this.content.iterator();
		while(contentParts.hasNext()){
			part.append(((QueryObject)(contentParts.next())).generateObjectString());
		}
		
		part.append(")");
		return part.toString();
	}
}
