/**
 * 
 */
package de.tarent.ldap.query;


/**
 * This class is an implementation of QueryObject which
 * simply takes a LDAP-query-string (which complies to RFC 4515,
 * "String Representation of Search Filters", http://tools.ietf.org/rfc/rfc4515.txt)
 * 
 * @author Fabian K&ouml;ster (f.koester@tarent.de) tarent GmbH Bonn
 *
 */
public class LdapQueryString implements QueryObject {
	
	private String ldapQueryString;
	
	public LdapQueryString(String ldapQueryString) {
		this.ldapQueryString = ldapQueryString;
	}

	/**
	 * @see de.tarent.ldap.query.QueryObject#generateObjectString()
	 */
	public String generateObjectString() throws InvalidQueryException {
		// TODO check if the given ldapQueryString is compliant to RFC 4515
		// and eventually throw an InvalidQueryException
		
		return ldapQueryString;
	}
}
