/*
 * LDAP-Proxy,
 * A lib to use LDAP
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'LDAP-Proxy'
 * Signature of Elmar Geese, 27 June 2007
 * Elmar Geese, CEO tarent GmbH.
 */
package de.tarent.ldap.query;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;


import de.tarent.ldap.LdapProxy;
import de.tarent.ldap.user.LdapUser;

public class LdapQuery implements QueryObject{
	
	private List parts;
	
	public LdapQuery(){
		parts = Collections.synchronizedList(new ArrayList());
	}
	
	public void addContent(QueryObject content){
		this.parts.add(content);
	}
	
	public QueryObject getContent(int index){
		return ((QueryObject)(this.parts.get(index)));
	}
	
	public String generateObjectString() throws InvalidQueryException{
		StringBuffer content = new StringBuffer();
		Iterator parts = this.parts.iterator();
		while(parts.hasNext()){
			content.append(((QueryObject)(parts.next())).generateObjectString());
		}
		return content.toString();
	}
}
