/*
 * LDAP-Proxy,
 * A lib to use LDAP
 * Copyright (C) 2000-2007 tarent GmbH
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License,version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 * 02110-1301, USA.
 *
 * tarent GmbH., hereby disclaims all copyright
 * interest in the program 'LDAP-Proxy'
 * Signature of Elmar Geese, 27 June 2007
 * Elmar Geese, CEO tarent GmbH.
 */

package de.tarent.ldap;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

/**
 * @author David Goemans, Tarent GmbH
 * 
 * This Class provides an interface to an LDAP-Systems.
 * It has a Hashtable which contains it's instances.
 */
public class LdapProxySecure extends LdapProxy{
    
	/** The user DN */
	private String user;
	
	/** Password of the directory User */
	private String pwd;
	
	/** Authorisation Type */
	private String authType;
	
	protected LdapProxySecure(){
		super();
	}
	
	protected LdapProxySecure(String host, String user, String pwd, String authType){
		super(host);
		this.user = user;
		this.pwd = pwd;
		this.authType = authType;
	}
	
	protected LdapProxySecure(String host, String user, String pwd, String authType, List searchPath){
		this(host, user, pwd, authType);
		this.searchPath = searchPath;
	}
	
	/**
	 * This method gets the Env-Table for an unbinded LDAPProxy
	 * 
	 * @return Environment Hashtable
	 */
	protected Hashtable getEnvHashTable(){
		return super.getSecureEnvHashTable(this.user, this.pwd, this.authType);
	}
	
}
